/* თვქენი დავალებაა გააკეთოთ character  ების კლასი სადაც შეინახავთ შემდეგ ინფორმაციას: სახელი და როლი (როლი შეიძლება იყოს რამოდენიმენაირი: mage, support, assassin, adc, tank 
(არ არის აუცილებელი ამ ჩამონათვლის სადმე შენახვა ან ყველა როლის გამოყენება)პ0ო, აქედან support mage  - ც არის.
ყველა პერსონაჟს სახელთან და როლთან ერთად აქვს armor ისა და damage ის მეთოდები, რომლებიც უბრალოდ ლოგავენ თუ რამდენი არმორი აქვს და რამდენი dmg აქვს პერსონაჟს.
damage ის შემთხვევაში შეგვიძლია გვააკეთოთ შემოწმება mage, support ზე და სხვა როლებზე, პირველი ორის შემთხვევაში პერსონაჟი magic damage ს აკეთებს,
ხოლო სხვა დანარჩენ შემთვევებში attack damage და მეთოდიც ამას ლოგავს.  mage ის კლასი ასევე მიიღებს magicPowers და აქვს ფრენის მეთოდიც (mage -ები დაფრინავენ კიდეც), 
რომელიც დალოგავს, რომ ამ მეიჯს შეუძლია ფრენაც. support იგივე მეიჯია, რომელიც მიიღებს heals - ს (თუ რამდენი "ერთეულის" დაჰილვა შეუძლია) ცვლადად და ექნნება  healing მეთოდი
რომელიც დალოგავს, რომ ამ გმირს შეუძლია გარკვეული რაოდენობის ერთეულის დაჰილვა. ასევე გვყავს adc რომელიც მიიღებს attackdamages ს და აქვს მეთოდი,
რომელიც ითვლის მის რეინჯს (მეთოდმა რეინჯის მნიშვნელობა უნდა მიიღოს), ამაზე დაყრდნობით ის ლოგავს ახლო რეინჯიანია პერსონაჟი თუ არა.
(პირობითად ვთქვათ, რომ 30 < ახლო რეინჯში ითვლება)


რაც შეეხება თვითონ პერსონაჟების სახელებს და ყველა იმ მნიშვნელობებს რომლებიც კლასებს უნდა გადასცეთ its up to you. თქვენი ამოცანა კლასების შექმნაში და მათი სტრუქტურის სწორ აგებულებაში მდგომარეობს.
*/

class Character {

    constructor(name, role) {
        this.name = name;
        this.role = role;
    }

    armor() {
        console.log(`${this.name} has armor`)
    }

    damage() {
        if(this.role === "mage" || role === "support") {
            console.log("magic damage");
        } else {
            console.log("physical damage")
        }
    }

    view() {
        console.log(`${this.name} is ${this.role}`)
    }
}
class core extends Character {
    constructor(name, damageRange) {
        super(name, "adc");
        this.damageRange = damageRange;
    }
    range() {
        if(this.damageRange > 50) {
            console.log("ranged")
        } else {
            console.log("melee")
        }
    }
}
class Mage extends Character {
    constructor(name) {
        super(name, "mage");
    }
    magicPower() {
        console.log(`${this.name} has the abillity to fly`)
    }
}
class Support extends Mage {
    constructor(name) {
        super(name, "support");
    }
    healing() {
        console.log(`${this.name} has the abillity to heal`)
    }
}

let charNana = new Support("Nana");
let charFanny = new core("Fanny", 40);
let charAtlas = new Character("Atlas", "tank");
let charLunox = new Mage("Lunox");
let charKhaled = new core("Khaled", 45);


charNana.view();
charFanny.view();
charAtlas.view();
charLunox.view();
charKhaled.view();

charNana.healing();
charLunox.magicPower();